using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class FakeDataLoader
        : IDataLoader
    {
        private CustomersList Customers { get; set; }
        private int rangeThread;

        public FakeDataLoader(CustomersList customers, int rangeThread)
        {
            Customers = customers;
            this.rangeThread = rangeThread;
        }

        public void LoadData()
        {
            List<Thread> threads = new List<Thread>();
            object _lock = new object();
            Stopwatch stopWatch = new Stopwatch();

            Console.WriteLine($"Loading data...{Customers.Customers.Count}");
            
            var pages = PagesCustomers(rangeThread);
            stopWatch.Start();

            foreach (var page in pages)
            {
                var thread = new Thread(() =>
                {
                    using (var db = new CustomerContext())
                    {
                        ICustomerRepository repos = new MSSQLCustomerRepository(db);
                        for (int i = 0; i < page.Count; i++)
                            page[i].Id = 0;
                        lock (_lock)
                        {
                            repos.AddCustomer(page);
                            repos.Save();
                        }
                    }
                })
                { IsBackground = true };
                thread.Start();
                threads.Add(thread);
            }

            foreach (var thread in threads)
                thread.Join();

            stopWatch.Stop();
            Console.WriteLine($"Loaded data...{stopWatch.Elapsed.TotalMilliseconds}");
        }

        public async void LoadDataAsync()
        {
            Console.WriteLine($"Loading data...{Customers.Customers.Count}");
            for (int i = 0; i < Customers.Customers.Count; i++)
                Customers.Customers[i].Id = 0;

            var pages = PagesCustomers(rangeThread);

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            foreach (var page in pages)
            {
                using (var db = new CustomerContext())
                {
                    ICustomerRepository repos = new MSSQLCustomerRepository(db);
                    await repos.AddCustomer(page);
                    await repos.Save();
                }
            }

            stopWatch.Stop();
            Console.WriteLine($"Loaded data...{stopWatch.Elapsed.TotalMilliseconds}");
        }

        private List<List<Customer>> PagesCustomers(int pages)
        {
            var pagesArr = new List<List<Customer>>();
            var page = new List<Customer>();
            var length = Math.Ceiling(Customers.Customers.Count / (double)pages);
            int count = 0;
            foreach (var item in Customers.Customers)
            {
                if (count < length)
                {
                    page.Add(item);
                    if (count == (length - 1))
                    {
                        pagesArr.Add(page);
                        page = new List<Customer>();
                        count = 0;
                    }
                    else
                        count += 1;
                }
                else
                    count = 0;
            }
            if (page.Count > 0)
                pagesArr.Add(page);
            return pagesArr;
        }
    }
}