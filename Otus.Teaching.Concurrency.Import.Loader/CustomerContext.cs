﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class CustomerContext : DbContext
    {
        public CustomerContext() : base()
        { }

        public DbSet<Customer> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString);
        }
    }
}
