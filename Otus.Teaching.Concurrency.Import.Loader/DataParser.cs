﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class DataParser : IDataParser<CustomersList>
    {
        private string path;

        public DataParser(string path)
        {
            this.path = path;
        }

        public CustomersList Parse()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomersList));
            if (path != null && path != "")
            {
                using (var fs = new FileStream(path, FileMode.OpenOrCreate))
                {
                    return (CustomersList)serializer.Deserialize(fs);
                }
            }
            else
                return new CustomersList();
        }
    }
}
