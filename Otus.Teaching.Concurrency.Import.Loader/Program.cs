﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");

        static void Main(string[] args)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                ArgumentList = { "wtf" },
                FileName = @"C:\Users\bmc09\OneDrive\Документы\Otus.Teaching.Concurrency.Import\Otus.Teaching.Concurrency.Import.DataGenerator.App\bin\Debug\netcoreapp3.1\Otus.Teaching.Concurrency.Import.DataGenerator.App.exe"
            };

            //Process.Start(startInfo);

            if (args != null && args.Length == 1)
            {
                if (args[0] == "0")
                    Process.Start(startInfo);
                else if (args[0] == "1")
                    GenerateCustomersDataFile();
            }
            else
                GenerateCustomersDataFile();

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            IDataParser<CustomersList> parse = new DataParser(_dataFilePath);
            var customers = parse.Parse();

            var loader = new FakeDataLoader(customers, 15);
            
            loader.LoadData();
            loader.LoadDataAsync();

            Console.ReadKey();
        }

        static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, 3_000);
            xmlGenerator.Generate();
        }
    }
}