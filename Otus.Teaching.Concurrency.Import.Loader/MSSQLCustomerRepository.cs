﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class MSSQLCustomerRepository : ICustomerRepository
    {
        private CustomerContext db;

        public MSSQLCustomerRepository(CustomerContext db)
        {
            this.db = db;
        }

        public async Task AddCustomer(List<Customer> customer)
        {
            await db.Customers.AddRangeAsync(customer);
        }

        public async Task Save()
        {
            await db.SaveChangesAsync();
        }
    }
}
