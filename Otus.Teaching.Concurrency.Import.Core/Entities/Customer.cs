using System;

namespace Otus.Teaching.Concurrency.Import.Handler.Entities
{
    [Serializable]
    public class Customer
    {
        public Customer()
        { }

        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }
    }
}